package com.anjuice.toutiao.dao;

import com.anjuice.toutiao.model.Comment;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author Anjuice
 * @create 2021-12-12 22:58
 */
@Mapper
public interface CommentDAO {

    String TABLE_NAME = " comment ";
    String INSERT_FIELDS = "user_id, content, created_date, entity_id, entity_type, status";
    String SELECT_FIELDS = " id, " + INSERT_FIELDS;

    @Insert({"insert into", TABLE_NAME, "(", INSERT_FIELDS, ") values (#{userId}, #{content}, " +
            "#{createdDate}, #{entityId}, #{entityType}, #{status})"})
    int addComment(Comment comment);

    @Update({"update", TABLE_NAME, "set status=#{status} where entity_id=#{entityId} and entity_type=#{entityType}"})
    void updateStatus(@Param("entityId") int entityId, @Param("entityType") int entityType, @Param("Status") int Status);

    @Select({"select", SELECT_FIELDS, "from ", TABLE_NAME, " where entity_type=#{entityType} and entity_id=#{entityId} order by id desc"})
    List<Comment> selectByEntity(@Param("entityId") int entityId, @Param("entityType") int entityType);

    @Select({"select count(id) from", TABLE_NAME, "where entity_type=#{entityType} and entity_id=#{entityId}"})
    int getCommentCount(@Param("entityId") int entityId, @Param("entityType") int entityType);

}
