package com.anjuice.toutiao.service;

import com.anjuice.toutiao.dao.LoginTicketDAO;
import com.anjuice.toutiao.dao.UserDAO;
import com.anjuice.toutiao.model.LoginTicket;
import com.anjuice.toutiao.model.User;
import com.anjuice.toutiao.util.ToutiaoUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author Anjuice
 * @create 2021-11-05 14:43
 */
@Service
public class UserService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private LoginTicketDAO loginTicketDAO;

    public User getUser(int id){
        return userDAO.selectById(id);
    }

//    注册

    public Map<String, Object> register(String username, String password){

        Map<String, Object> map = new HashMap<>();

        if (StringUtils.isBlank(username)){
            map.put("msgname", "用户名不能为空");
            return map;
        }

        if (StringUtils.isBlank(password)){
            map.put("msgpwd", "密码不能为空");
            return map;
        }

        User user = userDAO.selectByName(username);

        if (user != null){
            map.put("msgname", "用户名已经被注册");
            return map;
        }

        user = new User();
        user.setName(username);
        user.setSalt(UUID.randomUUID().toString().substring(0, 5));
        user.setHeadUrl(String.format("http://images.nowcoder.com/head/%dt.png", new Random().nextInt(1000)));

        user.setPassword(ToutiaoUtil.MD5(password + user.getSalt()));

        userDAO.addUser(user);

//        user.setPassword(md5(password));

        String ticket = addLoginTicket(user.getId());//private权限在类内部，可以调用；非静态方法直接调用非静态方法
        map.put("ticket", ticket);

        return map;

    }

//    登录

    public Map<String, Object> login(String username, String password){

        Map<String, Object> map = new HashMap<>();

        if (StringUtils.isBlank(username)){
            map.put("msgname", "用户名不能为空");
            return map;
        }

        if (StringUtils.isBlank(password)){
            map.put("msgpwd", "密码不能为空");
            return map;
        }

        User user = userDAO.selectByName(username);

        if (user == null){
            map.put("msgname", "用户名不存在");
            return map;
        }

        if (!ToutiaoUtil.MD5(password + user.getSalt()).equals(user.getPassword())){
            map.put("msgpwd", "密码不正确");
            return map;
        }

        map.put("userId", user.getId());

        //ticket
        String ticket = addLoginTicket(user.getId());//private权限在类内部，可以调用；非静态方法直接调用非静态方法
        map.put("ticket", ticket);

        return map;

    }

    private String addLoginTicket(int userId){
        LoginTicket ticket = new LoginTicket();
        ticket.setUserId(userId);
        Date date = new Date();
        date.setTime(date.getTime() + 1000 * 3600 * 24);
        ticket.setExpired(date);
        ticket.setStatus(0);
        ticket.setTicket(UUID.randomUUID().toString().replace("-", ""));
        loginTicketDAO.addTicker(ticket);
        return ticket.getTicket();
    }

    public void logout(String ticket){
        loginTicketDAO.updateStatus(ticket, 1);
    }

}
