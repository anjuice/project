package com.anjuice.toutiao.service;

import org.springframework.stereotype.Service;

/**
 * @author Anjuice
 * @create 2021-11-01 18:47
 */
@Service
public class ToutiaoService {
    public String say(){
        return "This is from ToutiaoService";
    }
}
