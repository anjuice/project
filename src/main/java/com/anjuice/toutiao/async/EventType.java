package com.anjuice.toutiao.async;


/**
 * @author Anjuice
 * @create 2022-01-16 22:40
 */
public enum EventType {

    LIKE(0),
    COMMENT(1),
    LOGIN(2),
    MAIL(3);

    private int value;

    EventType(int type){
        this.value = value;
    }
    public int getValue(){
        return value;
    }
}
