package com.anjuice.toutiao.async;

import java.util.List;

/**
 * @author Anjuice
 * @create 2022-01-16 23:07
 */
public interface EventHandler {

    void doHandle(EventModel model);
    List<EventType> getSupportEventTypes();

}
