package com.anjuice.toutiao.async.handler;


import com.anjuice.toutiao.async.EventHandler;
import com.anjuice.toutiao.async.EventModel;
import com.anjuice.toutiao.async.EventType;
import com.anjuice.toutiao.model.Message;
import com.anjuice.toutiao.model.User;
import com.anjuice.toutiao.service.MessageService;
import com.anjuice.toutiao.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Anjuice
 * @create 2022-01-16 23:09
 */
@Component
public class LikeHandler implements EventHandler{

    @Autowired
    MessageService messageService;

    @Autowired
    UserService userService;

    @Override
    public void doHandle(EventModel model) {

        System.out.println("Liked");
        Message message = new Message();
        int systemId = 3;
        message.setFromId(systemId);
//        message.setToId(model.getEntityOwnerId());//正确的做法
        message.setToId(model.getActorId());//方便演示给自己发
        User user = userService.getUser(model.getActorId());
        message.setContent("用户" + user.getName() + "赞了你的资讯，http://127.0.0.1:8080/news/" + model.getEntityId());
        message.setCreatedDate(new Date());
        message.setConversationId(systemId < model.getActorId() ? String.format("%d_%d", systemId, model.getActorId()) : String.format("%d_%d", model.getActorId(), systemId));
        messageService.addMessage(message);


    }

    @Override
    public List<EventType> getSupportEventTypes() {
        return Arrays.asList(EventType.LIKE);
    }


}
