package com.anjuice.toutiao.async.handler;

import com.anjuice.toutiao.async.EventHandler;
import com.anjuice.toutiao.async.EventModel;
import com.anjuice.toutiao.async.EventType;
import com.anjuice.toutiao.model.Message;
import com.anjuice.toutiao.service.MessageService;
import com.anjuice.toutiao.util.MailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author Anjuice
 * @create 2022-01-28 22:16
 */
@Component
public class LoginExceptionHandler implements EventHandler {

    @Autowired
    MessageService messageService;

    @Autowired
    MailSender mailSender;



    @Override
    public void doHandle(EventModel model) {
        //判断是否有异常登录
        Message message = new Message();
        int systemId = 3;
        message.setFromId(systemId);
        message.setToId(model.getActorId());
        message.setCreatedDate(new Date());
        message.setContent("你上次的登录ip异常");
        message.setConversationId(systemId < model.getActorId() ? String.format("%d_%d", systemId, model.getActorId()) : String.format("%d_%d", model.getActorId(), systemId));
        messageService.addMessage(message);

        Map<String, Object> map = new HashMap<>();
        map.put("username", model.getExt("username"));

        mailSender.sendWithHTMLTemplate(model.getExt("E-mail"),
                "登录异常",
                "mails/welcome.html", map);
    }

    @Override
    public List<EventType> getSupportEventTypes() {
        return Arrays.asList(EventType.LOGIN);
    }
}
