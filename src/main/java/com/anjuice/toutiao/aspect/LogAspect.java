package com.anjuice.toutiao.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author Anjuice
 * @create 2021-11-01 19:12
 */
@Aspect
@Component
public class LogAspect {

    private static final Logger logger = LoggerFactory.getLogger(LogAspect.class);

    @Before("execution(* com.anjuice.toutiao.controller.*Controller.*(..))")
    public void beforeMethod(JoinPoint joinPoint){

        StringBuilder sb = new StringBuilder();
        for (Object arg : joinPoint.getArgs()){
            sb.append("args:" + arg.toString() + "|");
        }
        logger.info("before method: " + sb.toString());
    }

    @After("execution(* com.anjuice.toutiao.controller.IndexController.*(..))")
    public void afterMethod(JoinPoint joinPoint){

        logger.info("after method: ");
    }

}
