package com.anjuice.toutiao.model;

import org.springframework.stereotype.Component;

/**
 * @author Anjuice
 * @create 2021-11-19 20:27
 */
@Component
public class HostHolder {// 存储这次访问里面的用户是谁

    private static ThreadLocal<User> users = new ThreadLocal<>();

    public User getUser(){
        return users.get();
    }

    public void setUser(User user){
        users.set(user);
    }

    public void clear(){
        users.remove();
    }

}
