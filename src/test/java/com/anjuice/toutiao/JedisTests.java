package com.anjuice.toutiao;

import com.anjuice.toutiao.model.*;
import com.anjuice.toutiao.util.JedisAdapter;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ToutiaoApplication.class)
//@WebAppConfiguration  这行会修改默认的启动路径需要注释掉
@Sql({"/init-schema.sql"})
public class JedisTests {

    @Autowired
    JedisAdapter jedisAdapter;

    @Test
    public void initData() {
        User user = new User();
        user.setHeadUrl("http://image.nowcoder.com/head/100t.png");
        user.setName("user1");
        user.setPassword("pwd");
        user.setSalt("salt");
        jedisAdapter.setObject("user1xx", user);
//        System.out.println(user);
// com.anjuice.toutiao.model.User@210308d5

        User u = jedisAdapter.getObject("user1xx", User.class);

//        System.out.println(u);
// com.anjuice.toutiao.model.User@3c5dbdf8
        System.out.println(ToStringBuilder.reflectionToString(u));
        //com.anjuice.toutiao.model.User@80bfdc6[id=0,name=user1,password=pwd,salt=salt,headUrl=http://image.nowcoder.com/head/100t.png]

    }
}
